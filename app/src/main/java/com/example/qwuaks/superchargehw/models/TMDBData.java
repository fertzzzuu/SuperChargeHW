package com.example.qwuaks.superchargehw.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TMDBData {
    @SerializedName("page")
    public int page;

    @SerializedName("total_results")
    public int totalResults;

    @SerializedName("total_pages")
    public int totalPages;

    @SerializedName("results")
    public List<Movie> results;




}
