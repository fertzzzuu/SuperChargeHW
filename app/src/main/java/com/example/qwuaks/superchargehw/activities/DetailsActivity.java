package com.example.qwuaks.superchargehw.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.qwuaks.superchargehw.R;
import com.example.qwuaks.superchargehw.models.Genre;
import com.example.qwuaks.superchargehw.models.Movie;
import com.example.qwuaks.superchargehw.utils.TMDBBackendUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DetailsActivity extends AppCompatActivity {

    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        int id = getIntent().getIntExtra("id", 0);
        TMDBBackendUtil tmdbBackendUtil = new TMDBBackendUtil(this);
        tmdbBackendUtil.setMovie(id);

    }

    public void setMovie(Movie movie) {
        this.movie = movie;
        TextView title = findViewById(R.id.title);
        title.setText(movie.title);
        TextView runtime= findViewById(R.id.runtime);
        runtime.setText(getResources().getString(R.string.runtime_string, movie.runtime));
        TextView genres = findViewById(R.id.genres);
        StringBuilder genresBuilder = new StringBuilder();
        for (Genre genre :
                movie.genres) {
            genresBuilder.append(genre.name).append(" ");
        }
        genres.setText(genresBuilder.toString());
        RatingBar rate = findViewById(R.id.ratingBar);
        rate.setRating((float) movie.voteAverage / 2);
        TextView releaseDate = findViewById(R.id.release_date);
        releaseDate.setText(movie.releaseDate);
        String path = "https://image.tmdb.org/t/p/w500" + movie.posterPath;
        ImageView posterView = findViewById(R.id.posterView);
        Picasso.with(this)
                .load(path).fit()
                .centerInside()
                .error(R.drawable.ic_image_black_24dp)
                .placeholder(R.drawable.ic_image_black_24dp)
                .into(posterView);

        TextView overview = findViewById(R.id.overview);
        overview.setText(movie.overview);


    }


}
