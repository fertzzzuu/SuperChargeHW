package com.example.qwuaks.superchargehw.misc;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.qwuaks.superchargehw.R;
import com.example.qwuaks.superchargehw.activities.DetailsActivity;
import com.example.qwuaks.superchargehw.models.Movie;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qwuaks on 2017.10.29..
 */

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {

    private List<Movie> movies;

    public MovieAdapter(List<Movie> movies) {
        this.movies = movies;
    }

    public MovieAdapter() {
        this.movies = new ArrayList<>();
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        this.notifyDataSetChanged();
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.movieRating.setText(String.valueOf(movies.get(position).voteAverage));
        holder.movieTitle.setText(movies.get(position).title);
        holder.movieRatingBar.setRating((float) (movies.get(position).voteAverage / 2.0));
        String path = "https://image.tmdb.org/t/p/w500" + movies.get(position).posterPath;
        Picasso.with(holder.movieImg.getContext()).load(path)
                .fit().centerCrop()
                .error(R.drawable.ic_image_black_24dp)
                .placeholder(R.drawable.ic_image_black_24dp)
                .into(holder.movieImg);

    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public void addMovies(List<Movie> movies) {
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder {
        private ImageView movieImg;
        private TextView movieTitle;
        private TextView movieRating;
        private RatingBar movieRatingBar;


        public MovieViewHolder(final View itemView) {
            super(itemView);
            movieImg = itemView.findViewById(R.id.movie_image);
            movieTitle = itemView.findViewById(R.id.movie_title);
            movieRating = itemView.findViewById(R.id.movie_rating);
            movieRatingBar = itemView.findViewById(R.id.movie_rating_bar);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Movie selectedMovie = movies.get(getAdapterPosition());
                    Log.d("MovieAdapter:", "onClick: " + selectedMovie.title);
                    Intent intent = new Intent(itemView.getContext(), DetailsActivity.class);
                    intent.putExtra("id", selectedMovie.id);
                    itemView.getContext().startActivity(intent);
                }
            });
        }


    }
}
