package com.example.qwuaks.superchargehw.utils;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.example.qwuaks.superchargehw.activities.BrowseActivity;
import com.example.qwuaks.superchargehw.activities.DetailsActivity;
import com.example.qwuaks.superchargehw.models.Movie;
import com.example.qwuaks.superchargehw.models.TMDBData;

import java.io.IOException;
import java.util.List;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TMDBBackendUtil {

    private static final String API_URL = "https://api.themoviedb.org/3/search/movie";
    public static final String API_KEY = "43a7ea280d085bd0376e108680615c7f";
    public static final String BASE_URL = "https://api.themoviedb.org/3/";
    private Activity activity;
    private TMDBService tmdbService;

    public TMDBBackendUtil(Activity activity) {
        this.activity = activity;
        OkHttpClient.Builder client = new OkHttpClient.Builder();

        client.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Interceptor.Chain chain) throws IOException {
                Request request = chain.request();
                HttpUrl url = request.url().newBuilder().addQueryParameter("api_key", API_KEY).build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build();

        tmdbService = retrofit.create(TMDBService.class);
    }




    public void getTrendingMovies() {
        Call<TMDBData> trendingCall = tmdbService.getTrendingMovies();
        trendingCall.enqueue(new Callback<TMDBData>() {
            @Override
            public void onResponse(Call<TMDBData> call, retrofit2.Response<TMDBData> response) {
                if (response.isSuccessful()) {
                    TMDBData data = response.body();
                    ((BrowseActivity) activity).refresh(data.results);
                }
            }

            @Override
            public void onFailure(Call<TMDBData> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }


    public void setMovie(int id) {
        Call<Movie> movieCall = tmdbService.getMovie(id);
        movieCall.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, retrofit2.Response<Movie> response) {
                if (response.isSuccessful()) {
                    ((DetailsActivity)activity).setMovie(response.body());
                }
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }

    public void getRandomMovie(int page, final int releaseDate, int voteAverage){
        Call<TMDBData> randomCall = tmdbService.getRandomMovie(page, releaseDate, voteAverage);
        randomCall.enqueue(new Callback<TMDBData>() {
            @Override
            public void onResponse(Call<TMDBData> call, retrofit2.Response<TMDBData> response) {
                if (response.isSuccessful()){
                    TMDBData data = response.body();
                    ((BrowseActivity)activity).showRandomMovie(data.results.get(0));
                }
            }

            @Override
            public void onFailure(Call<TMDBData> call, Throwable t) {
            }
        });
    }

    public void searchMovie(String q){
        Call<TMDBData> searchCall = tmdbService.searchMovies(q);
        searchCall.enqueue(new Callback<TMDBData>() {
            @Override
            public void onResponse(Call<TMDBData> call, retrofit2.Response<TMDBData> response) {
                if (response.isSuccessful()){
                    TMDBData data = response.body();
                    ((BrowseActivity)activity).refresh(data.results);
                }
            }

            @Override
            public void onFailure(Call<TMDBData> call, Throwable t) {

            }
        });
    }


    public void loadMore(int page) {
        BrowseActivity.page++;
        Call<TMDBData> moreCall = tmdbService.getMoreMovies(page);
        moreCall.enqueue(new Callback<TMDBData>() {
            @Override
            public void onResponse(Call<TMDBData> call, retrofit2.Response<TMDBData> response) {
                if (response.isSuccessful()){
                    TMDBData data = response.body();
                    ((BrowseActivity)activity).addMore(data.results);
                }
            }

            @Override
            public void onFailure(Call<TMDBData> call, Throwable t) {

            }
        });
    }
}