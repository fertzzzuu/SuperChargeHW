package com.example.qwuaks.superchargehw.utils;

import com.example.qwuaks.superchargehw.models.Movie;
import com.example.qwuaks.superchargehw.models.TMDBData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TMDBService {

    @GET("trending/movie/week")
    Call<TMDBData> getTrendingMovies();

    @GET("trending/movie/week")
    Call<TMDBData> getMoreMovies(@Query("page") int page);

    @GET("movie/{movie_id}")
    Call<Movie> getMovie(@Path("movie_id") int id);

    @GET("discover/movie")
    Call<TMDBData> getRandomMovie(@Query("page") int page, @Query("primary_release_year") int releasedate,
                                  @Query("vote_average.gte") int voteAverage);

    @GET("search/movie")
    Call<TMDBData> searchMovies(@Query("query") String q);


}
