package com.example.qwuaks.superchargehw.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;


public class Movie {
    public String title;
    public int id;
    public int runtime;
    public boolean adult;
    public String overview;

    @SerializedName("genres")
    public List<Genre> genres;

    @SerializedName("poster_path")
    public String posterPath;

    @SerializedName("vote_average")
    public double voteAverage;

    @SerializedName("vote_count")
    public int voteCount;

    @SerializedName("release_date")
    public String releaseDate;


}
