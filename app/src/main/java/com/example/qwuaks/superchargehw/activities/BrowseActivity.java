package com.example.qwuaks.superchargehw.activities;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.qwuaks.superchargehw.R;
import com.example.qwuaks.superchargehw.misc.MovieAdapter;
import com.example.qwuaks.superchargehw.models.Movie;
import com.example.qwuaks.superchargehw.utils.TMDBBackendUtil;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class BrowseActivity extends AppCompatActivity {

    private List<Movie> movies;
    private RecyclerView recyclerView;
    private TMDBBackendUtil backendUtil;
    static public int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse);
        recyclerView = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);

        ImageButton searchButton = findViewById(R.id.searchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String q = ((EditText)findViewById(R.id.editText)).getText().toString();
                if (!q.isEmpty())
                    backendUtil.searchMovie(q);
                else
                    backendUtil.getTrendingMovies();
            }
        });

        recyclerView.setAdapter(new MovieAdapter());
        this.backendUtil = new TMDBBackendUtil(this);
        backendUtil.getTrendingMovies();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRandomMovie();
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1)) {
                    loadMore(page);

                }
            }
        });

    }

    public void getRandomMovie(){
        Random random = new Random();
        int page = random.nextInt(1000) + 1;
        int currYear = Calendar.getInstance().get(Calendar.YEAR);
        int releaseDate = random.nextInt(currYear - 1970) + 1970;
        int voteAverage = random.nextInt(4) + 6;
        backendUtil.getRandomMovie(page, releaseDate, voteAverage);

    }

    public void refresh(List<Movie> trendingMovies) {
        ((MovieAdapter)recyclerView.getAdapter()).setMovies(trendingMovies);
    }

    public void showRandomMovie(Movie m){
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("id", m.id);
        startActivity(intent);
    }

    public void loadMore(int page){
        backendUtil.loadMore(page);
    }

    public void addMore(List<Movie> movies){
        ((MovieAdapter)recyclerView.getAdapter()).addMovies(movies);
    }

}
